def list_movies(request):
    movie_list = Movie.objects.all()
    context = {'movie_list': movie_list}
    return render(request, 'movies/index.html', context)

def create_movie(request):
    if request.method == 'POST':
        movie_name = request.POST['name']
        movie_release_year = request.POST['release_year']
        movie_poster_url = request.POST['poster_url']
        movie = Movie(name=movie_name,
                      release_year=movie_release_year,
                      poster_url=movie_poster_url)
        movie.save()
        return HttpResponseRedirect(
            reverse('movies:detail', args=(movie.id, )))
    else:
        
        return render(request, 'movies/create.html', {})

def create_movie(request):
    if request.method == 'POST':
        movie_name = request.POST['name']
        movie_release_year = request.POST['release_year']
        movie_poster_url = request.POST['poster_url']
        movie = Movie(name=movie_name,
                      release_year=movie_release_year,
                      poster_url=movie_poster_url)
        movie.save()
        return HttpResponseRedirect(
            reverse('movies:detail', args=(movie.id, )))
    else:
        form = MovieForm()
        context = {'form': form}
        return render(request, 'movies/create.html', context)


def update_movie(request, movie_id):
    movie = get_object_or_404(Movie, pk=movie_id)

    if request.method == "POST":
        movie.name = request.POST['name']
        movie.release_year = request.POST['release_year']
        movie_poster_url = request.POST['poster_url']
        movie.save()
        return HttpResponseRedirect(
            reverse('movies:detail', args=(movie.id, )))

    context = {'movie': movie}
    return render(request, 'movies/update.html', context)