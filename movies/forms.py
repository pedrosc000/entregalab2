from django.forms import ModelForm
from .models import Movie, Review
from django import forms

class MovieForm(ModelForm):
    class Meta:
        model = Movie
        fields = [
            'name',
            'release_year',
            'poster_url',
        ]
        labels = {
            'name': 'Título',
            'release_year': 'Ingredientes',
            'poster_url': 'URL da Receita',
        }


class ReviewForm(ModelForm):
    class Meta:
        model = Review
        fields = [
            'author',
            'text',
        ]
        labels = {
            'author': 'Usuário',
            'text': 'Comentário',
        }