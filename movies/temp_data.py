movie_data = [{
    "id":
    "1",
    "name":
    "Pipoca",
    "release_year":
    "Ingredientes: ½ xícara (chá) de milho para pipoca, ½ colher (sopa) de óleo, sal a gosto",
    "poster_url":
    "https://imgur.com/a/bSBTWjy"
}, {
    "id":
    "2",
    "name":
    "Omelete",
    "release_year":
    "Ingredientes: 2 ovos, 1 pitada de sal, 1 fatia de presunto, 2 fatias de queijo ",
    "poster_url":
    "https://imgur.com/a/Rl2LNxx"
}, {
    "id":
    "3",
    "name":
    "Arroz",
    "release_year":
    "Ingredientes: 1 xícara de arroz, 2 xícaras de água, 1/2 cebola, 1 colher de azeite, 1/2 colher de sal ",
    "poster_url":
    "https://imgur.com/a/Jkauvga"
}]
